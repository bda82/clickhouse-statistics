from abc import ABC, abstractmethod


class BaseSeeder(ABC):

    def __init__(self):
        pass

    @abstractmethod
    def run(self, params: dict):
        raise NotImplementedError
