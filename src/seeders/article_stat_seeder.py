from .base_seeder import BaseSeeder

from models import ArticleStatistics


class ArticleStatisticsSeeder(BaseSeeder):

    def run(self, params: dict = None):
        print('Create ArticleStatistics table')
        ArticleStatistics.db.create_table(ArticleStatistics)
