from .clickhouse_config import ClickHouseConfig


class Config:

    def __init__(self):
        self.server_port = 3000
        self.clickhouse = ClickHouseConfig()
