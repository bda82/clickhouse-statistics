import os


class ClickHouseConfig:

    def __init__(self):
        self.db = os.environ.get('CLICKHOUSE_DB', 'clickhouse')
        self.host = os.environ.get('CLICKHOUSE_HOST', 'localhost')
        self.port = os.environ.get('CLICKHOUSE_PORT', 8123)
        self.user = os.environ.get('CLICKHOUSE_USER', 'root')
        self.password = os.environ.get('CLICKHOUSE_PASSWORD', 'password')
        self.url = f'http://{self.host}:{self.port}/'
