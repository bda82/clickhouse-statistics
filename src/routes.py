from handlers import *


def setup_routes(app):
    app.router.add_view('/set-stat', SetStatHandler)
    app.router.add_view('/get-stat', GetStatHandler)
    app.router.add_view('/seed-clickhouse', SeedDataHandler)
