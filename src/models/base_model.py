from infi.clickhouse_orm import Database, Model

from config import *

clichouse_config = Config().clickhouse


class BaseModel(Model):
    db = Database(db_name=clichouse_config.db,
                  db_url=clichouse_config.url,
                  username=clichouse_config.user,
                  password=clichouse_config.password)
