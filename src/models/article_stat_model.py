import uuid
from infi.clickhouse_orm import (
    MergeTree,
    UUIDField,
    UInt32Field,
    UInt8Field,
    Float32Field,
    DateTimeField,
    StringField
)

from .base_model import BaseModel


class ArticleStatistics(BaseModel):
    id = UUIDField(default=uuid.uuid1())
    ts = DateTimeField()
    article_id = UInt32Field()
    url = StringField()
    read = UInt32Field()
    exit = UInt32Field()
    popularity = Float32Field()
    completed = UInt8Field()

    engine = MergeTree(partition_key=['toYYYYMM(ts)'],
                       order_by=('id', 'ts'))
