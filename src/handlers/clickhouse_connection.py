import asyncio
from config import Config
from infi.clickhouse_orm import Database

from seeders import *

config = Config().clickhouse


class ClickHouseConnector:
    status = 'offline'

    def __init__(self):
        super(ClickHouseConnector, self).__init__()
        self.attempt = 0
        self.num_of_attempts = 10
        self.time_out = 30
        self.connected = False
        self.db = Database(db_name=config.db,
                           db_url=config.url,
                           username=config.user,
                           password=config.password)

    async def test_connection(self):
        self.connected = False
        for self.attempt in range(self.num_of_attempts):
            try:
                raw = self.db.raw("SELECT now(), version();").strip()
                print(f'ClickHouse raw query "SELECT now(), version();" answer is {raw}')
                if raw:
                    self.connected = True
            except Exception as e:
                print(f'{e}')
                self.connected = False
            if self.connected:
                break
            print(f'ClickHouse Connection attempt # {self.attempt}...')
            await asyncio.sleep(self.time_out)
        print(f'ClickHouse Connection result is {self.connected}')
        if not self.connected:
            raise Exception(f'ClickHouse Connection result is {self.connected}')
        return self.connected

    async def create_tables(self):
        await asyncio.sleep(1)
        ArticleStatisticsSeeder().run()
        return True
