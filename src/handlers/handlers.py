import random
import json
import datetime
from json import JSONDecodeError

from aiohttp import web
from aiohttp_cors import CorsViewMixin

from infi.clickhouse_orm import F

from models import *


def current_datetime(days=0, hours=0, minutes=0, seconds=0):
    return datetime.datetime.now() - datetime.timedelta(days=days,
                                                        hours=hours,
                                                        minutes=minutes,
                                                        seconds=seconds)


def get_completed_article_read():
    return list(ArticleStatistics.objects_in(ArticleStatistics.db).filter(
        ArticleStatistics.completed == 1
    ).aggregate(read=F.sum(ArticleStatistics.read)))[0]


def get_completed_article_exit():
    return list(ArticleStatistics.objects_in(ArticleStatistics.db).filter(
        ArticleStatistics.completed == 1
    ).aggregate(exit=F.sum(ArticleStatistics.exit)))[0]


def get_uncompleted_article_read():
    return list(ArticleStatistics.objects_in(ArticleStatistics.db).filter(
        ArticleStatistics.completed == 0
    ).aggregate(read=F.sum(ArticleStatistics.read)))[0]


def get_uncompleted_article_exited():
    return list(ArticleStatistics.objects_in(ArticleStatistics.db).filter(
        ArticleStatistics.completed == 0
    ).aggregate(exit=F.sum(ArticleStatistics.exit)))[0]


def get_average_read():
    return list(ArticleStatistics.objects_in(ArticleStatistics.db)
                .aggregate(read=F.avg(ArticleStatistics.read)))[0]


def get_average_exited():
    return list(ArticleStatistics.objects_in(ArticleStatistics.db)
                .aggregate(exit=F.avg(ArticleStatistics.exit)))[0]


def get_read_exited_one_day_past():
    return list(ArticleStatistics.objects_in(ArticleStatistics.db).filter(
        ArticleStatistics.completed == 0,
        ArticleStatistics.ts < current_datetime(days=1)
    ).aggregate(read=F.sum(ArticleStatistics.read),
                exit=F.sum(ArticleStatistics.exit)))[0]


class SetStatHandler(web.View, CorsViewMixin):

    async def post(self):
        try:
            request = await self.request.json()
        except JSONDecodeError:
            request_str = await self.request.text()
            request = json.loads(request_str)

        if not request:
            raise web.HTTPBadRequest()

        print(f'{request = }')

        # request = {
        #   'article_id': 9,
        #   'url': 'https://testsite.com/articles/9',
        #   'read': 84,
        #   'exit': 7,
        #   'popularity': 0.08333333333333333,
        #   'completed': 1
        # }

        data_template = {
            "ts": str(current_datetime()),
            "article_id": request['article_id'],
            "url": request['url'],
            "read": request['read'],
            "exit": request['exit'],
            "popularity": round(float(request['exit'] / request['read']), 3) if request['read'] != 0 else 0.0,
            "completed": request['completed']
        }
        ch_data = ArticleStatistics(**data_template)
        if ch_data:
            ArticleStatistics.db.insert([ch_data])

        response = {'data': request}

        status_code = web.HTTPCreated().status_code

        return web.json_response(data=response, status=status_code)


class GetStatHandler(web.View, CorsViewMixin):

    async def get(self):
        response = {'data': {}}

        response['data']['completed_article_read'] = get_completed_article_read().read
        response['data']['completed_article_exit'] = get_completed_article_exit().exit
        response['data']['uncompleted_article_read'] = get_uncompleted_article_read().read
        response['data']['uncompleted_article_exited'] = get_uncompleted_article_exited().exit
        response['data']['average_read'] = get_average_read().read
        response['data']['average_exited'] = get_average_exited().exit
        read_exit = get_read_exited_one_day_past()
        response['data']['read_one_day_past'] = read_exit.read
        response['data']['exit_one_day_past'] = read_exit.exit

        status_code = web.HTTPOk().status_code

        return web.json_response(data=response, status=status_code)


class SeedDataHandler(web.View, CorsViewMixin):

    async def get(self):
        response = {'data': []}

        # Seed Data into ClickHouse
        data_for_ch = []
        number = random.randint(1, 10)
        delay_days = random.randint(0, 3)
        for _ in range(1, number):
            article_id = random.randint(1, 10)
            read = random.randint(1, 100)
            exited = random.randint(1, 10)
            completed = 1 if random.randint(1, 100) > 60 else 0
            data_template = {
                "ts": str(current_datetime(days=delay_days)),
                "article_id": article_id,
                "url": "https://testsite.com/articles/" + str(article_id),
                "read": read,
                "exit": exited,
                "popularity": round(float(exited / read), 3) if read != 0 else 0.0,
                "completed": completed
            }
            data_for_ch.append(ArticleStatistics(**data_template))
            response['data'].append(data_template)
        if data_for_ch:
            ArticleStatistics.db.insert(data_for_ch)

        status_code = web.HTTPCreated().status_code

        return web.json_response(data=response, status=status_code)