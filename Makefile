build:
	docker-compose up --build
run:
	make build
	docker-compose up -d
down:
	docker-compose down
rl:
	make run
	docker logs ex_clickhouse -f