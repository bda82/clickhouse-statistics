FROM python:3.8-slim-buster

ARG DEBIAN_FRONTEND=noninteractive

ENV TZ=Europe/Moscow
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN apt-get update && \
    apt-get install -y --no-install-recommends git && \
    apt-get install -y --no-install-recommends build-essential && \
    apt-get purge -y --auto-remove && \
    rm -rf /var/lib/apt/lists/* && \
    pip install -U pip --no-cache-dir && \
	pip install --no-cache-dir wheel

WORKDIR /app

COPY ./requirements.txt ./requirements.txt
RUN	pip install --no-cache-dir -r requirements.txt

COPY ./src .

CMD ["python3", "/app/main.py"]
