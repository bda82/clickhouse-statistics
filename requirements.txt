aiohttp==3.7.4.post0
async-timeout==3.0.1
aiohttp-cors>=0.7.0
uvloop>=0.16.0
infi.clickhouse-orm==2.1.0
PyYAML==5.4.1
